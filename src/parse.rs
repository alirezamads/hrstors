use std::io::*;
use crate::write_file::WriteRustFile;

const TAG_OPEN: &str = "<?";
const TAG_CLOSE: &str = "?>";
const SET_HEADER: &str = "Web::set_content_type(TXT_HTML);";

pub struct Parse;

impl Parse
{
    pub fn parse(_html_file: std::fs::File, _write: &WriteRustFile)
    {
        // local variables ---------------------------------------------------------------------- //
        let mut _reader                     = std::io::BufReader::new(_html_file);  // File reader 
        let mut _string_flag: bool           = false;   // Am i in string ?
        let mut _tag_flag: bool              = false;   // Am i in tag ?
        let mut _normal_line: bool          = true;    // is it normal line ? 
        let mut _src_data                   = String::new();
        let mut _lines: usize               = 0;       
        let mut _tag_lines: usize           = 0;
        let mut _read_chars: usize          = 0;

        let mut _bracket_depth: u8          = 0;
        let mut _bracket_depth_vec: Vec<u8> = Vec::new();
        let mut _is_header_set: bool        = false;
        // -------------------------------------------------------------------------------------- //


        loop // Read Lines Loop ------------------------------------------------------------------ //
        {
            // Read Lines -------------------------------- //
            _lines = _lines + 1; //File Lines..
            let mut _html_data = String::new();
            _reader.read_line(&mut _html_data).unwrap();
            if _html_data.len() == 0 // end of file 
            {
                break;
            }
            _html_data.push('\n');
            // ------------------------------------------- //
            // println!("|{}|", _lines);
            _read_chars = 0;
            loop // Read Chars Loop -------------------------------------------------------------- //
            {
                // println!("Line: {}, BD: {}",_lines , _bracket_depth);
                // loop counter and break --------------- //
                if _read_chars >= _html_data.chars().count() - 1
                {
                    break;
                }
                // -------------------------------------- //

                // Stat 1: TAG OPEN ------------------------------------------ //
                if &_html_data[_read_chars .. _read_chars + 2] == TAG_OPEN 
                    && _tag_flag == false 
                    && _string_flag == false
                {
                    // println!("HI{} ", _lines);
                    _read_chars = _read_chars + 2;
                    _tag_flag = true;
                    continue;
                }
                // ----------------------------------------------------------- //

                // Stat 2: BRACKET BLOCK ---------------------------------- //
                if _string_flag == false && _tag_flag == true
                {
                    if _html_data.find(SET_HEADER) != None && _is_header_set == false
                    {
                        _is_header_set = true;
                        _bracket_depth_vec.push(_bracket_depth);
                        // println!("FIND {} in {}", _lines, _bracket_depth );
                    }
                    // Open {
                    if _html_data.chars().nth(_read_chars).unwrap() == '{' 
                    {
                        _bracket_depth += 1;
                        if _is_header_set == true
                        {
                            _is_header_set = false;
                        }
                    }
                    // Close }
                    if _html_data.chars().nth(_read_chars).unwrap() == '}' 
                    {
                        _bracket_depth -= 1;
                        if _bracket_depth_vec.get(0) > Some(&_bracket_depth)
                        { 
                            _bracket_depth_vec.pop();
                        }
                    }
                }
                // ----------------------------------------------------------- //

                // Stat 3: ENTER & EXIT of STRING IN TAG --------------------- //
                if _html_data.chars().nth(_read_chars + 1).unwrap() == '\"'    
                    && _html_data.chars().nth(_read_chars).unwrap() != '\\'    
                    && _tag_flag == true // in tag <? .. ?>                                      
                {
                    _string_flag = _string_flag ^! false;
                }
                // ----------------------------------------------------------- //


                // Stat 4: IN TAG -------------------------------------------- //
                if _tag_flag == true
                {
                    // UNTILL TAG CLOASE FOUND and NOT IN STRING -------- //
                    if &_html_data[_read_chars .. _read_chars + 2] == TAG_CLOSE 
                        && _string_flag == false 
                    {
                        // println!("{}",_src_data);
                        Parse::html_parsed_write_to_file(&mut _src_data, _write);
                        _normal_line = false;
                        _read_chars = _read_chars + 2;
                        _tag_flag = false;
                        
                        // println!("set Header: {}", _is_header_set);
                        _src_data.clear();
                        break;
                    }
                    else 
                    {
                        _src_data.push(_html_data.chars().nth(_read_chars).unwrap());
                    }
                }
                // ----------------------------------------------------------- //
                
                // Stat 5: WRITE HEADER -------------------------------------- //
                if _tag_flag == false && _is_header_set == false
                {
                    // println!("--- {} - {} ---", _lines, _bracket_depth);
                    // // agar laye haye zirin set nashode budan
                    // println!("P{:?}",  _bracket_depth_vec);
                    
                    if _bracket_depth_vec.len() == 0
                    {
                        _write.write_to_file(&SET_HEADER.to_string());
                        _bracket_depth_vec.push(_bracket_depth);
                    }

                    if _bracket_depth_vec.get(0) > Some(&_bracket_depth)
                    { 
                        _write.write_to_file(&SET_HEADER.to_string());
                        _bracket_depth_vec.pop();
                    }
                    _is_header_set = true;
                }
                // ----------------------------------------------------------- //
                _read_chars = _read_chars + 1;
            } // Read Chars Loop ----------------------------------------------------------------- //
            // stat 4: NORMAL STATMENTS ---------------------------------- //
            if _tag_flag == false && _normal_line == true
            {
                // print!("{}",_html_data);
                Parse::html_write_to_file(&mut _html_data, _write);
            }
            _normal_line = true;
            // ----------------------------------------------------------- //
        } // Read Lines Loop --------------------------------------------------------------------- //
    }

    fn html_parsed_write_to_file(_parse_html:&mut String, _write: &WriteRustFile)
    {
        if _parse_html.chars().nth(0).unwrap() == '='
        {
            _parse_html.remove(0); // remove '='
            _parse_html.insert_str(0, "println!(\"{}\", " );
            _parse_html.push_str(");");
        }
        _write.write_to_file(&_parse_html);
    }

    fn html_write_to_file(_html: &mut String, _write: &WriteRustFile)
    {
        if _html.chars().last().unwrap() == '\n'
            { _html.pop(); }
        if _html.chars().last().unwrap() == '\n'
            { _html.pop(); }
        let mut _ok_html_line = _html.replace("\"", "\\\"");
        _ok_html_line = _ok_html_line.replace("{", "{{");
        _ok_html_line = _ok_html_line.replace("}", "}}");
        _ok_html_line.insert_str(0, "println!(\"");
        _ok_html_line.push_str("\");");

        _write.write_to_file(&_ok_html_line);
    }
}