use std::io::{BufWriter, Write};
use std::fs::OpenOptions;
use std::path::*;

pub struct WriteRustFile
{
    _path: String,
}

impl WriteRustFile
{
    pub fn path(_path_in: &String) -> WriteRustFile
    {
        let mut _path_ex = PathBuf::from(&_path_in);
        _path_ex.set_extension("rs");
        let _rs_file_path = String::from(_path_ex.to_string_lossy());
        WriteRustFile
        {
            _path: _rs_file_path
        }
    }

    pub fn creat_file(&self, _name: &String)
    {
        let mut _file = std::fs::File::create(&self._path).expect("Unable to create file");
        _file.write_all("fn main()\n{\n".as_bytes()).unwrap();
    }
    
    pub fn write_to_file(&self, _str: &String)
    {
        let _file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(&self._path)
            .expect("Unable to open file");

        let mut _write = BufWriter::new(_file);
        writeln!(_write, "{}", _str).unwrap();
    }

    pub fn end_of_rust_file(&self)
    {
        let _file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(&self._path)
            .expect("Unable to open file");

        let mut _write = BufWriter::new(_file);
        writeln!(_write, "{}", "}").unwrap();
    }
}