use std::path::Path;
use std::ffi::OsStr;

pub fn get_extension_from_filename(filename: &str) -> &str
{
    Path::new(filename)
        .extension()
        .and_then(OsStr::to_str)
        .unwrap()
}

pub fn read_file(_name: String) -> std::fs::File
{
    let file = std::fs::File::open(_name).expect("Unable to open file html");
    file
}