mod read_file;
mod parse;
mod write_file;


use crate::read_file::read_file;
use crate::parse::Parse;
use crate::write_file::WriteRustFile;


fn main()
{
    let _args: Vec<String> = std::env::args().collect();
    if _args.len() == 1
    {
        panic!("Missing [.hrs] file path");
    }
    let path = &_args[1];
    let path_to_check_extension = path;

    // Chack Extension .hrs ------------------------------- //
    if read_file::get_extension_from_filename(path_to_check_extension) != "hrs"
    {
        panic!("Its not .hrs file");
    }

    let mut _to_write = WriteRustFile::path(&path);

    let _html_directory = String::from(path);
    
    _to_write.creat_file(&path);
    Parse::parse(
        read_file(_html_directory),
        &_to_write
    );
    _to_write.end_of_rust_file();
}



// https://www.youtube.com/watch?v=ThNTJFBYL0Q